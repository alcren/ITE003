/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 6
 * PROJECT NAME/CLASS NAME: PRELIM/LARGERVALUE
 * DATE: 4/20/16
 */
import java.util.Scanner;
public class LargerValue
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Input first number: ");
        int num1 = sc.nextInt();
        System.out.print("Input second number: ");
        int num2 = sc.nextInt();
        System.out.print("Input third number: ");
        int num3 = sc.nextInt();
        int largest = -2147483648;
        if (num1 >= num2 && num1 >= num3 ){
            largest = num1;
        } else if (num2 >= num1 && num2 >= num3){
            largest = num2;
        } else if (num3 >= num1 && num3 >= num2){
            largest = num3;
        }
        if (num1 == num2 && num1 == num3)
            System.out.println("All values are equal.");
        if (largest != -2147483648)
            System.out.println(largest + " has largest value.");
    }
}
