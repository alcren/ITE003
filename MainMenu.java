/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 15
 * PROJECT NAME/CLASS NAME: PRELIM/MAINMENU
 * DATE: 05/17/16
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
public class MainMenu extends JFrame
{
    JLabel menuL;
    JLabel authorL;

    JButton rectB;
    JButton circB;
    JButton exitB;

    RectButtonHandler rbHandler;
    CircButtonHandler cbHandler;
    ExitButtonHandler ebHandler;

    public MainMenu(){
        setTitle("Main Menu");

        menuL = new JLabel("Main Menu", JLabel.CENTER);
        menuL.setForeground(new Color(16, 13, 89));
        authorL = new JLabel("Programmed by: EPMJ", JLabel.CENTER);
        authorL.setForeground(new Color(16, 13, 89));
        
        rectB = new JButton("Rectangle");
        rbHandler = new RectButtonHandler();
        rectB.addActionListener(rbHandler);
        rectB.setBackground(new Color(55, 52, 128));
        rectB.setForeground(new Color(255, 177, 0));
        
        circB = new JButton("Circle");
        cbHandler = new CircButtonHandler();
        circB.addActionListener(cbHandler);
        circB.setBackground(new Color(55, 52, 128));
        circB.setForeground(new Color(255, 177, 0));

        exitB = new JButton("Exit");
        ebHandler = new ExitButtonHandler();
        exitB.addActionListener(ebHandler);
        exitB.setBackground(new Color(55, 52, 128));
        exitB.setForeground(new Color(255, 177, 0));
        
        Container pane = getContentPane();
        pane.setBackground(new Color(126, 124, 171));
        JPanel buttonPanel = new JPanel();
        pane.setLayout(new GridLayout(3,1));
        pane.add(menuL);
        buttonPanel.setLayout(new GridLayout(1,3));
        buttonPanel.add(rectB);
        buttonPanel.add(circB);
        buttonPanel.add(exitB);
        pane.add(buttonPanel);
        pane.add(authorL);
        setSize(400,200);
        setVisible(true);
    }

    public class RectButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            RectangleGUI rectObj = new RectangleGUI();
            setVisible(false);
        }
    }
    public class CircButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            CircleAreaGUI circObj = new CircleAreaGUI();
            setVisible(false);
        }
    }
    public class ExitButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }
    public static void main (String[] args){
        MainMenu menuObj = new MainMenu();
    }
}
