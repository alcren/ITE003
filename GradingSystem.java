/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 7
 * PROJECT NAME/CLASS NAME: PRELIM/GRADINGSYSTEM
 * DATE: 4/20/16
 */
import java.util.Scanner;
public class GradingSystem
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("***POINT GRADE CALCULATOR***");
        System.out.print("Enter grade in quiz 1: ");
        int quiz_1 = sc.nextInt();
        System.out.print("Enter grade in quiz 2: ");
        int quiz_2 = sc.nextInt();
        System.out.print("Enter grade in quiz 3: ");
        int quiz_3 = sc.nextInt();
        System.out.print("Enter grade in quiz 4: ");
        int quiz_4 = sc.nextInt();
        double ave = (quiz_1 + quiz_2 + quiz_3 + quiz_4) / 4;
        System.out.println("Average grade: " + ave);
        double point_grade = 5.00;
        if (ave > 94.00 && ave <= 100){
            point_grade = 1.00;
        }
        else if (ave > 49.99 && ave <= 55.49){
            point_grade = 3.00;
        }
        else if (ave > 55.50 && ave <= 60.99){
            point_grade = 2.75;
        }
        else if (ave > 61.00 && ave <= 65.49){
            point_grade = 2.50;
        }
        else if (ave > 65.50 && ave <= 71.99){
            point_grade = 2.25;
        }
        else if (ave > 72.00 && ave <= 77.49){
            point_grade = 2.00;
        }
        else if (ave > 77.50 && ave <= 82.99){
            point_grade = 1.75;
        }
        else if (ave > 83.00 && ave <= 88.49){
            point_grade = 1.50;
        }
        else if (ave > 88.50 && ave <= 93.99){
            point_grade = 1.25;
        }
        else 
            point_grade = 5.00;
        System.out.println("Your point grade is " + point_grade);
    }
}
