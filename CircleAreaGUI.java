/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 15
 * PROJECT NAME/CLASS NAME: PRELIM/CIRCLEAREAGUI
 * DATE: 05/17/16
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
public class CircleAreaGUI extends JFrame
{
    JLabel radiusL;
    JLabel circumferenceL;
    JLabel areaL;

    JTextField radiusTF;
    JTextField circumferenceTF;
    JTextField areaTF;

    JButton calculateB;
    JButton exitB;

    CalculateButtonHandler cbHandler;
    ExitButtonHandler ebHandler;

    public CircleAreaGUI(){
        setTitle("Area and Perimeter of Circle");

        radiusL = new JLabel("Enter radius: ", JLabel.CENTER);
        radiusL.setForeground(new Color(246, 193, 166));
        circumferenceL = new JLabel("Circumference: ", JLabel.CENTER);
        circumferenceL.setForeground(new Color(246, 193, 166));
        areaL = new JLabel("Area: ", JLabel.CENTER);
        areaL.setForeground(new Color(246, 193, 166));

        radiusTF = new JTextField();
        radiusTF.setBackground(new Color(184, 132, 56));
        circumferenceTF = new JTextField();
        circumferenceTF.setBackground(new Color(184, 132, 56));
        circumferenceTF.setEditable(false);
        areaTF = new JTextField();
        areaTF.setBackground(new Color(184, 132, 56));
        areaTF.setEditable(false);

        calculateB = new JButton("Calculate");
        calculateB.setBackground(new Color(169, 51, 78));
        calculateB.setForeground(new Color(226, 152, 169));
        cbHandler = new CalculateButtonHandler();
        calculateB.addActionListener(cbHandler);

        exitB = new JButton("Exit");
        exitB.setBackground(new Color(169, 51, 78));
        exitB.setForeground(new Color(226, 152, 169));
        ebHandler = new ExitButtonHandler();
        exitB.addActionListener(ebHandler);

        Container pane = getContentPane();
        pane.setBackground(new Color(184, 99, 56));
        pane.setLayout(new GridLayout(4,2));
        pane.add(radiusL);
        pane.add(radiusTF);
        pane.add(circumferenceL);
        pane.add(circumferenceTF);
        pane.add(areaL);
        pane.add(areaTF);
        pane.add(calculateB);
        pane.add(exitB);
        setSize(400,400);
        setVisible(true);
    }

    public class CalculateButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            double radius = Double.parseDouble(radiusTF.getText());
            DecimalFormat df = new DecimalFormat("#.##");
            double circumference = Double.parseDouble(df.format(2 * 3.14 * radius));
            double area = Double.parseDouble(df.format(3.14 * radius * radius));
            circumferenceTF.setText("" + circumference);
            areaTF.setText("" + area);
        }
    }
    public class ExitButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            setVisible(false);
            MainMenu menu = new MainMenu();
        }
    }
    public static void main (String[] args){
        CircleAreaGUI circleObj = new CircleAreaGUI();
    }
}
