import java.io.*;
public class Average
{
    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter three numbers: ");
        try {
            double x = Double.parseDouble(br.readLine());
            double y = Double.parseDouble(br.readLine());
            double z = Double.parseDouble(br.readLine());
            double ave = (x + y + z) / 3;
            System.out.println("The average of the three numbers is: " + ave);
        } catch(IOException e){}
    }
}
