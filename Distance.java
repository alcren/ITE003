import java.io.*;
public class Distance
{
    public static void main (String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try{
            System.out.println("Enter time: " );
            double time = Double.parseDouble(br.readLine());
            System.out.println("Enter speed: ");
            double speed= Double.parseDouble(br.readLine());
            double distance=speed*time;
            System.out.println("Distance: " + distance);
        } catch(IOException e){}
    }
}
