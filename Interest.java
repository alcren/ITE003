import java.util.Scanner;
public class Interest
{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter starting money (PHP): ");
        double p = sc.nextDouble();
        System.out.println("Enter interest rate (in percentage):");
        double r = sc.nextDouble();
        r = r / 100;
        System.out.println("Enter how long is the money borrowed (in years): ");
        double t = sc.nextDouble();
        double i = p * r * t;
        System.out.println("The interest is: " + i + " PHP");
    }
}
