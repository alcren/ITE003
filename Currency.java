/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 4
 * PROJECT NAME/CLASS NAME: PRELIM/CURRENCY
 * DATE: 4/19/16
 */
import java.util.Scanner;
public class Currency
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("\t***Welcome to currency converter!***");
        System.out.print("\nEnter amount in PHP: ");
        double php = sc.nextDouble(); 
        double usd = php * 0.0216389; //us dollar
        double jpy = php * 2.366190;  //japanese yen
        double cny = php * 0.140400;  //chinese yuan
        double myr = php * 0.084741;  //malaysian ringgit
        double sgd = php * 0.029304;  //singaporean dollar
        double sar = php * 0.081297;  //saudi arabian riyal
        double gbp = php * 0.015176;  //british pound
        double cad = php * 0.027719;  //canadian dollar
        double mxn = php * 0.377778;  //mexican peso
        double eur = php * 0.019180;  //euro
        double aed = php * 0.079665;  //united arab emirates dirham
        double afn = php * 1.48293;  //afghanistan afghani
        double aud = php * 0.027900;  //australian dollar
        double ars = php * 0.304277;  //argentinian peso
        double brl = php * 0.078381;  //brazil real
        System.out.println("\n" + php + " PHP is equivalent to:");
        System.out.println(">> " + usd + " US Dollars.");
        System.out.println(">> " + jpy + " Japanese Yens.");
        System.out.println(">> " + cny + " Chinese Yuans.");
        System.out.println(">> " + myr + " Malaysian Ringgits.");
        System.out.println(">> " + sgd + " Singaporean Dollars.");
        System.out.println(">> " + sar + " Saudi Arabian Riyals.");
        System.out.println(">> " + gbp + " British Pounds.");
        System.out.println(">> " + cad + " Canadian Dollars.");
        System.out.println(">> " + mxn + " Mexican Pesos.");
        System.out.println(">> " + eur + " Euro.");
        System.out.println(">> " + aed + " Emirati Dirhams.");
        System.out.println(">> " + afn + " Afghan Afghanis.");
        System.out.println(">> " + aud + " Australian Dollars.");
        System.out.println(">> " + ars + " Argentinian Pesos.");
        System.out.println(">> " + brl + " Brazilian Reals.");
    }
}
