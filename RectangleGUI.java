/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 15
 * PROJECT NAME/CLASS NAME: PRELIM/RECTANGLEGUI
 * DATE: 05/17/16
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
public class RectangleGUI extends JFrame
{
    JLabel lengthL;
    JLabel widthL;
    JLabel areaL;

    JTextField lengthTF;
    JTextField widthTF;
    JTextField areaTF;

    JButton calculateB;
    JButton exitB;

    CalculateButtonHandler cbHandler;
    ExitButtonHandler ebHandler;

    public RectangleGUI(){
        setTitle("Area of Rectangle");

        lengthL = new JLabel("Enter length: ", JLabel.CENTER);
        widthL = new JLabel("Enter width: ", JLabel.CENTER);
        areaL = new JLabel("Area: ", JLabel.CENTER);

        lengthTF = new JTextField();
        lengthTF.setBackground(new Color(246, 246, 166));
        widthTF = new JTextField();
        widthTF.setBackground(new Color(246, 246, 166));
        areaTF = new JTextField();
        areaTF.setBackground(new Color(246, 246, 166));
        areaTF.setEditable(false);

        calculateB = new JButton("Calculate");
        cbHandler = new CalculateButtonHandler();
        calculateB.addActionListener(cbHandler);
        calculateB.setBackground(new Color(127, 127, 0));
        
        exitB = new JButton("Exit");
        ebHandler = new ExitButtonHandler();
        exitB.addActionListener(ebHandler);
        exitB.setBackground(new Color(127, 127, 0));

        Container pane = getContentPane();
        pane.setBackground(new Color(184, 184, 56));
        pane.setLayout(new GridLayout(4,2));
        pane.add(lengthL);
        pane.add(lengthTF);
        pane.add(widthL);
        pane.add(widthTF);
        pane.add(areaL);
        pane.add(areaTF);
        pane.add(calculateB);
        pane.add(exitB);
        setSize(400,400);
        setVisible(true);
    }

    public class CalculateButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            double length = Double.parseDouble(lengthTF.getText());
            double width = Double.parseDouble(widthTF.getText());
            DecimalFormat df = new DecimalFormat("#.##");
            double area = Double.parseDouble(df.format(length * width));
            areaTF.setText("" + area);
        }
    }
    public class ExitButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            setVisible(false);
            MainMenu menu = new MainMenu();
        }
    }
    public static void main (String[] args){
        RectangleGUI rectObj = new RectangleGUI();
    }
}
