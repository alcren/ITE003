/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 3
 * PROJECT NAME/CLASS NAME: PRELIM/FUNDOPER
 * DATE: 4/19/16
 */
import java.util.Scanner;
public class FundOper
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("\t***FUNDAMENTAL OPERATIONS***");
        System.out.print("\n\nEnter first integer: ");
        int a = sc.nextInt(); 
        System.out.print("Enter second integer: ");
        int b = sc.nextInt(); 
        int sum = a + b;
        int diff = a - b;
        int prod = a * b;
        int quo = a / b;
        int mod = a % b;
        float ave = (float) sum / 2;
        System.out.println("\nThe sum of " + a + " and " + b + " is " + sum);
        System.out.println("The difference of " + a + " and " + b + " is " + diff);
        System.out.println("The product of " + a + " and " + b + " is " + prod);
        System.out.println("The quotient of " + a + " and " + b + " is " + quo);
        System.out.println("The remainder of " + a + " divided by " + b + " is " + mod);
        System.out.println("The average  of " + a + " and " + b + " is " + ave);
    }
}
