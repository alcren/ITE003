/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 5
 * PROJECT NAME/CLASS NAME: PRELIM/SHAPES
 * DATE: 4/19/16
 */
import java.util.Scanner;
public class Shapes
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("\t***PERIMETER AND AREA CALCULATOR***");

        //square
        System.out.print("\n\n\t[SQUARE]");
        System.out.print("\n\nEnter length of the sides: ");
        double sqr_l = sc.nextDouble(); 
        double sqr_per = 4 * sqr_l;
        double sqr_area = sqr_l * sqr_l;
        System.out.println("The perimeter of the square is " + sqr_per);
        System.out.println("The area of the square is " + sqr_area);

        //circle
        System.out.print("\n\n\t[CIRCLE]");
        System.out.print("\n\nEnter radius of the circle: ");
        double r = sc.nextDouble();
        final double PI = 3.14;
        double cir_per = 2 * PI * r;
        double cir_area = PI * r * r;
        System.out.println("The perimeter of the circle is " + cir_per);
        System.out.println("The area of the circle is " + cir_area);

        //rectangle
        System.out.print("\n\n\t[RECTANGLE]");
        System.out.print("\n\nEnter length of the rectangle: ");
        double rec_l = sc.nextDouble();
        System.out.print("Enter width of the rectangle: ");
        double rec_w = sc.nextDouble();
        double rec_per = 2 * rec_l + 2 * rec_w;
        double rec_area = rec_l * rec_w;
        System.out.println("The perimeter of the rectangle is " + rec_per);
        System.out.println("The area of the rectangle is " + rec_area);

        //triangle
        System.out.print("\n\n\t[TRIANGLE]");
        System.out.print("\n\nEnter length of side A: ");
        double tri_a = sc.nextDouble();
        System.out.print("Enter length of side B: ");
        double tri_b = sc.nextDouble();
        System.out.print("Enter length of side C: ");
        double tri_c = sc.nextDouble();
        double tri_per = tri_a + tri_b + tri_c;
        System.out.println("The perimeter of the triangle is " + tri_per);
        System.out.print("\nEnter base of the triangle: ");
        double tri_base = sc.nextDouble();
        System.out.print("Enter height of the triangle: ");
        double tri_h = sc.nextDouble();
        double tri_area = .5 * tri_base * tri_h;
        System.out.println("The area of the triangle is " + tri_area);

        //parallelogram
        System.out.print("\n\n\t[PARALLELOGRAM]");
        System.out.print("\n\nEnter base of the parallelogram: ");
        double par_b = sc.nextDouble();
        System.out.print("Enter side of the parallelogram: ");
        double par_s = sc.nextDouble();
        double par_per = 2 * par_b + 2 * par_s;
        System.out.println("The perimeter of the parallelogram is " + par_per);
        System.out.print("\nEnter height of the parallelogram: ");
        double par_h = sc.nextDouble();
        System.out.print("Enter base of the parallelogram: ");
        par_b = sc.nextDouble();
        double par_area = par_b * par_h;
        System.out.println("The area of the parallelogram is " + par_area);

        //rhombus
        System.out.print("\n\n\t[RHOMBUS]");
        System.out.print("\n\nEnter length of the sides: ");
        double rho_s = sc.nextDouble();
        double rho_per = 4 * rho_s;
        System.out.println("The perimeter of the parallelogram is " + rho_per);
        System.out.print("\nEnter the length of diagonal 1: ");
        double rho_p = sc.nextDouble();
        System.out.print("Enter length of diagonal 2: ");
        double rho_q = sc.nextDouble();
        double rho_area = rho_p * rho_q / 2;
        System.out.println("The area of the parallelogram is " + rho_area);

        //trapezoid
        System.out.print("\n\n\t[TRAPEZOID]");
        System.out.print("\n\nEnter length of base A: ");
        double tra_a = sc.nextDouble();
        System.out.print("Enter length of base B: ");
        double tra_b = sc.nextDouble();
        System.out.print("Enter length of side C: ");
        double tra_c = sc.nextDouble();
        System.out.print("Enter length of side D: ");
        double tra_d = sc.nextDouble();
        double tra_per = tra_a + tra_b + tra_c + tra_d;
        System.out.println("The perimeter of the trapezoid is " + tra_per);
        System.out.print("\nEnter length of base A: ");
        tra_a = sc.nextDouble();
        System.out.print("Enter length of base B: ");
        tra_b = sc.nextDouble();
        System.out.print("Enter height of the trapezoid: ");
        double tra_h = sc.nextDouble();
        double tra_area = (tra_a + tra_b) * tra_h / 2;
        System.out.println("The area of the trapezoid is " + tra_area);

    }
}
