/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 16
 * PROJECT NAME/CLASS NAME: PRELIM/NUMBERSYSTEM
 * DATE: 05/05/16
 */
import java.util.Scanner;
public class NumberSystem
{

    public static void main (String[] args){
        int x = 1;
        while(x>0){
            System.out.println("\t<<MAIN MENU>>");
            System.out.println("[1] Decimal to Binary");
            System.out.println("[2] Binary to Decimal");
            System.out.println("[3] Decimal to Octal");
            System.out.println("[4] Octal to Decimal");
            System.out.println("[5] Decimal to Hex");
            System.out.println("[6] Hex to Decimal");
            System.out.println("[7] Exit\n");
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter OPTION: ");
            char opt = sc.next().charAt(0);
            System.out.print('\u000c');
            switch (opt){
                case '1' :
                decToBin();
                break;
                case '2' :
                binToDec();
                break;
                case '3' :
                decToOct();
                break;
                case '4' :
                octToDec();
                break;
                case '5' :
                decToHex();
                break;
                case '6' :
                hexToDec();
                break;
                case '7' :
                x = 0;
                break;
                default :
                continue;
            } 
            if(x==1){
                System.out.print("\nPress 1 to go back to Main Menu, 2 for Exit: ");
                int repeat = sc.nextInt();
                if(repeat == 1)
                    System.out.print('\u000c');
                else{
                    System.out.print('\u000c');
                    x = 0;
                }
            }
        }
    }
    //DECIMAL TO BINARY
    public static void decToBin()
    {
        System.out.println("\t<<Decimal to Binary>>");
        System.out.print("Enter decimal value: ");
        Scanner sc = new Scanner(System.in);
        int decimal = sc.nextInt();
        int result = convertDecimalToBase(decimal, 2);
        System.out.println ("Binary Equivalent: " + result);
    }
    //DECIMAL TO OCTAL
    public static void decToOct()
    {
        System.out.println("\t<<Decimal to Octal>>");
        System.out.print("Enter decimal value: ");
        Scanner sc = new Scanner(System.in);
        int decimal = sc.nextInt();
        int result = convertDecimalToBase(decimal, 8);
        System.out.println ("Octal Equivalent: " + result);
    }
    //DECIMAL TO ANY BASE
    public static int convertDecimalToBase(int decimal , int base)
    {
        int result = 0;
        int multiplier = 1;
        while(decimal > 0)
        {
            int residue = decimal % base;
            decimal     = decimal / base;
            result      = result + residue * multiplier;
            multiplier  = multiplier * 10;
        }
        return result;
    }
    //BINARY TO DECIMAL
    public static void binToDec(){
        System.out.println("\t<<Binary to Decimal>>");
        System.out.print("Enter binary value: ");
        Scanner sc = new Scanner(System.in);
        int binnum, decnum=0, i=1, rem;
        binnum = sc.nextInt();
        while(binnum != 0)
        {
            rem = binnum%10;
            decnum = decnum + rem*i;
            i = i*2;
            binnum = binnum/10;
        }
        System.out.println("Decimal Equivalent: " + decnum);
    }
    //OCTAL TO DECIMAL
    public static void octToDec()
    {
        System.out.println("\t<<Octal to Decimal>>");
        int octnum, decnum=0, i=0, orig;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter Octal Number : ");
        octnum = scan.nextInt();
        orig = octnum;
        while(octnum != 0)
        {
            decnum = decnum + (octnum%10) * (int) Math.pow(8, i);
            i++;
            octnum = octnum/10;
        }
        System.out.println("Decimal Equivalent: " + decnum);
    }
    //DECIMAL TO HEX
    public static void decToHex(){
        System.out.println("\t<<Decimal to Hexadecimal>>");
        int decnum, rem;
        String hexdecnum="";
        /* digits in hexadecimal number system */
        char hex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter Decimal Number : ");
        decnum = scan.nextInt();
        while(decnum>0)
        {
            rem = decnum%16;
            hexdecnum = hex[rem] + hexdecnum;
            decnum = decnum/16;
        }
        System.out.println("Hexadecimal Equivalent: " + hexdecnum);
    }
    //HEX TO DECIMAL
    public static void hexToDec(){
        System.out.println("\t<<Hexadecimal to Decimal>>");
        String hexdecnum;
        int decnum;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter Hexadecimal Number : ");
        hexdecnum = scan.nextLine();
        String digits = "0123456789ABCDEF";
        hexdecnum = hexdecnum.toUpperCase();
        int val = 0;
        for (int i = 0; i < hexdecnum.length(); i++)
        {
            char c = hexdecnum.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        decnum = val;
        System.out.println("Decimal Equivalent: " + decnum);
    }
}
