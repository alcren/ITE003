/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 12
 * PROJECT NAME/CLASS NAME: PRELIM/STARTINGANDENDINGVALUE
 * DATE: 4/26/16
 */
import java.util.Scanner;
public class StartingAndEndingValue
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter starting value: ");
        int start = sc.nextInt();
        System.out.print("Enter ending value: ");
        int end = sc.nextInt();
        System.out.print("Enter interval: ");
        int interval = sc.nextInt();
        int i = 0;
        System.out.println("===========================================");
        if(start <= end){
            for(i = start; i<=end; i+=interval){
                System.out.print(i + " ");
            }
        } else{
            for(i = start; i>=end; i-=interval){
                System.out.print(i + " ");
            }
        } 
    }
}
