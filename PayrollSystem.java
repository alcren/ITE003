/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 8
 * PROJECT NAME/CLASS NAME: PRELIM/PAYROLLSYSTEM
 * DATE: 4/20/16
 */
import java.util.Scanner;
public class PayrollSystem
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("***NET PAY CALCULATOR***");
        System.out.print("Days worked: ");
        double days = sc.nextInt();
        System.out.print("Rate per day of employee: ");
        double rate = sc.nextInt();
        double gross = days * rate;
        System.out.println("Gross Pay: " + gross);
        double tax = 0;
        if (gross > 30001){
            tax = .25;
        }
        else if (gross >= 20001 && gross <= 30000){
            tax = .20;
        }
        else if (gross >= 10000 && gross <= 20000){
            tax = .15;
        }
        else
            tax = 0;
        double netPay = gross - (gross * tax);
        System.out.println("Tax: " + (gross * tax));
        System.out.println("Net Pay: " + netPay);
    }
}
