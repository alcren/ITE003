/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 15
 * PROJECT NAME/CLASS NAME: PRELIM/ORDERINGSYSTEM
 * DATE: 05/17/16
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
public class OrderingSystem extends JFrame
{
    JLabel itemL;
    JLabel priceL;
    JLabel qtyL;
    JLabel amtL;
    JLabel cashL;
    JLabel changeL;
    JLabel titleL;
    JLabel authorL;

    JComboBox itemList;

    JTextField priceTF;
    JTextField qtyTF;
    JTextField amtTF;
    JTextField changeTF;
    JTextField cashTF;

    JButton calculateAB;
    JButton calculateCB;
    JButton clearB;
    JButton exitB;

    ItemSelectionHandler itemHandler;

    CalculateAButtonHandler cabHandler;
    CalculateCButtonHandler ccbHandler;
    ClearButtonHandler clrHandler;
    ExitButtonHandler ebHandler;

    public OrderingSystem(){
        setTitle("Ordering System");

        String[] items = {"Samsung Galaxy S7", "Huawei P9", "Iphone 6S 64GB", "HTC 10", "Samsung Galaxy Note 5"};
        JComboBox itemList = new JComboBox(items);
        itemList.setBackground(new Color(246, 246, 166));
        itemList.setSelectedIndex(4);
        itemHandler = new ItemSelectionHandler();
        itemList.addActionListener(itemHandler);

        titleL = new JLabel("Ordering System", JLabel.CENTER);
        authorL = new JLabel("Programmed by: Efren Mercado Jr", JLabel.CENTER);
        itemL = new JLabel("Choose items", JLabel.CENTER);
        priceL = new JLabel("Price", JLabel.CENTER);
        qtyL = new JLabel("Quantity", JLabel.CENTER);
        amtL = new JLabel("Amount", JLabel.CENTER);
        cashL = new JLabel("Cash Tendered", JLabel.CENTER);
        changeL = new JLabel("Change", JLabel.CENTER);

        priceTF = new JTextField("26200");
        priceTF.setBackground(new Color(246, 246, 166));
        priceTF.setEditable(false);
        qtyTF = new JTextField();
        qtyTF.setBackground(new Color(246, 246, 166));
        amtTF = new JTextField();
        amtTF.setBackground(new Color(246, 246, 166));
        amtTF.setEditable(false);
        cashTF = new JTextField();
        cashTF.setBackground(new Color(246, 246, 166));
        changeTF = new JTextField();
        changeTF.setBackground(new Color(246, 246, 166));
        changeTF.setEditable(false);

        calculateAB = new JButton("Calculate Amount");
        cabHandler = new CalculateAButtonHandler();
        calculateAB.addActionListener(cabHandler);
        calculateAB.setBackground(new Color(127, 127, 0));

        calculateCB = new JButton("Calculate Change");
        ccbHandler = new CalculateCButtonHandler();
        calculateCB.addActionListener(ccbHandler);
        calculateCB.setBackground(new Color(127, 127, 0));

        clearB = new JButton("Clear");
        clrHandler = new ClearButtonHandler();
        clearB.addActionListener(clrHandler);
        clearB.setBackground(new Color(127, 127, 0));

        exitB = new JButton("Exit");
        ebHandler = new ExitButtonHandler();
        exitB.addActionListener(ebHandler);
        exitB.setBackground(new Color(127, 127, 0));

        Container pane = getContentPane();
        JPanel main = new JPanel();
        pane.setBackground(new Color(220, 220, 56));
        pane.setLayout(new GridLayout(3,1));
        pane.add(titleL);
        pane.add(main);
        pane.add(authorL);
        main.setBackground(new Color(184, 184, 56));
        main.setLayout(new GridLayout(8,2));
        main.add(itemL);
        main.add(itemList);
        main.add(priceL);
        main.add(priceTF);
        main.add(qtyL);
        main.add(qtyTF);
        main.add(amtL);
        main.add(amtTF);
        main.add(cashL);
        main.add(cashTF);
        main.add(changeL);
        main.add(changeTF);
        main.add(calculateAB);
        main.add(calculateCB);
        main.add(clearB);
        main.add(exitB);
        setSize(800,800);
        setVisible(true);
    }
    public class ItemSelectionHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            JComboBox cb = (JComboBox)e.getSource();
            String itemName = (String)cb.getSelectedItem();
            if(itemName == "Samsung Galaxy S7")
                priceTF.setText("27900");
            else if(itemName == "Huawei P9")
                priceTF.setText("30500");
            else if(itemName == "Iphone 6S 64GB")
                priceTF.setText("38000");
            else if(itemName == "HTC 10")
                priceTF.setText("34500");
            else if(itemName == "Samsung Galaxy Note 5")
                priceTF.setText("26200");
        }
    }
    public class CalculateAButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            double qty = Double.parseDouble(qtyTF.getText());
            double price = Double.parseDouble(priceTF.getText());
            double amt = qty * price;
            amtTF.setText("" + amt);
        }
    }
    public class CalculateCButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            double cash = Double.parseDouble(cashTF.getText());
            double amount = Double.parseDouble(amtTF.getText());
            double change = cash - amount;
            if(change < 0)
                changeTF.setText("Insufficient payment");
            else
                changeTF.setText("" + change);
        }
    }
    public class ClearButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            priceTF.setText(null);
            qtyTF.setText(null);
            amtTF.setText(null);
            cashTF.setText(null);
            changeTF.setText(null);
        }
    }
    public class ExitButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }
    public static void main (String[] args){
        OrderingSystem orderObj = new OrderingSystem();
    }
}
