/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 15
 * PROJECT NAME/CLASS NAME: PRELIM/MAGICSQUARE
 * DATE: 05/03/16
 */
import java.util.Scanner;
public class MagicSquare
{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("\t<< MAGIC SQUARE >>\n  ");
        System.out.println(" ");
        int[][] square = new int[3][3];
        int[] totalrow = {0, 0, 0};
        int[] totalcol = {0, 0, 0};
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                System.out.print("Enter element " + i + " " + j + ": ");
                square[i][j] = sc.nextInt();
            }
            System.out.print("\n");
        }
        boolean isMagic = true;
        //check rows
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                totalrow[i] += square[i][j];
            }
            if(totalrow[i] != 15){
                isMagic = false;
            }
        }
        //check columns
        for(int j = 0; j < 3; j++){
            for(int i = 0; i < 3; i++){
                totalcol[j] += square[i][j];
            }
            if(totalcol[j] != 15){
                isMagic = false;
            }
        }
        //check diagonals
        int dtotal1 = 0;
        int dtotal2 = 0;
        int j = 2;
        for(int i = 0; i < 3; i++){
            dtotal1 += square[i][i];
            dtotal2 += square[j][i];
            j--;
        }
        if(dtotal1 != 15 || dtotal2 != 15)
            isMagic = false;

        System.out.println("Here is your input: ");
        for(int i = 0; i < 3; i++){
            System.out.print("\t");
            for(int k = 0; k < 3; k++){
                System.out.print(square[i][k] + "\t");
            }
            System.out.print(totalrow[i]);
            System.out.print("\n");
        }
        System.out.print(dtotal2 + "\t");
        for(int i = 0; i < 3; i++){
            System.out.print(totalcol[i] + "\t");
        }
        System.out.print(dtotal1);
        if( isMagic == true)
            System.out.println("\n\nIt is a magic square!");
        else
            System.out.println("\n\nIt is not a magic square!");
    }
}
