/*
 * Title: ProfileAndCS
 * Author: Efren Mercado Jr
 * Program: BS CS
 * Year Level: First Year
 * Date: 4/14/16
 */
public class ProfileAndCS
{
    public static void main (String[] args){
       double quiz = 80, activity = 96, hwAndSw = 67;
       double classStanding = quiz * .6 + activity * .2 + hwAndSw * .2;
       
       System.out.println("Name: Efren Mercado Jr");
       System.out.println("Program: BS Computer Science");
       System.out.println("Year Level: First Year");
       System.out.println("Quizzes: " + quiz);
       System.out.println("Activity: " + activity);
       System.out.println("HW/SW: " + hwAndSw);
       System.out.println("Class Standing: " + classStanding);
    }}
