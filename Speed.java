import java.io.*;
public class Speed
{

    public static void main (String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the value for distance: ");
        try {
            double distance = Double.parseDouble(br.readLine());
            System.out.println("Enter value for time: ");
            double time = Double.parseDouble(br.readLine());
            double speed = distance / time;
            System.out.println("Speed: " + speed);
        } catch(IOException e){}
    }
}
