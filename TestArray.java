import java.util.Scanner;
public class TestArray
{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int[] myList = new int[10];
        for(int i = 0; i < myList.length; i++){
            System.out.print("Enter element " + (i+1) + ": ");
            myList[i] = sc.nextInt();
        }
        double total = 0;
        double max = myList[0];
        double min = myList[0];
        System.out.print("The elements are ");
        for(int i = 0; i < myList.length; i++){
            System.out.print(myList[i] + " ");
            if(myList[i] > max) max = myList[i];
            if(myList[i] < min) min = myList[i];
            total += myList[i];
        }
        double ave = total / myList.length;
        System.out.println("\nTotal is " + total + " ");
        System.out.println("Maximum is " + max);
        System.out.println("Minimum is " + min);
        System.out.println("Average is " + ave);
        bubbleSort(myList);
        System.out.print("The sorted elements are ");
        for(int i = 0; i < myList.length; i++){
            System.out.print(myList[i] + " ");
        }
    }

    public static void bubbleSort(int[] myList) {

        int n = myList.length;
        int temp = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {

                if (myList[j - 1] > myList[j]) {
                    temp = myList[j - 1];
                    myList[j - 1] = myList[j];
                    myList[j] = temp;
                }

            }
        }
    }
}
