
/**
 * Write a description of class Hello here.
 * 
 * @author Efren Mercado Jr
 * @version 04/14/16
 */
public class Hello
{
    /*
    this is the main method of the program
    Displays Hello World in the terminal
    */
    public static void main (String[] args){
        System.out.println("Hello World");
    }
}
