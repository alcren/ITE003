/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 17
 * PROJECT NAME/CLASS NAME: PRELIM/GUI
 * DATE: 5/12/16
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class GUI extends JFrame
{
    JLabel firstNameLbl;
    JLabel lastNameLbl;
    JLabel emailLbl;

    JButton generate;
    JButton exit;
    JTextField firstNameTxt, lastNameTxt, emailTxt;

    generateBtnHandler generateHandler;
    exitBtnHandler exitHandler;

    public GUI(){
        setTitle("Test");
        firstNameLbl = new JLabel("Enter first name: ", JLabel.CENTER);
        lastNameLbl = new JLabel("Enter last name: ", JLabel.CENTER);
        emailLbl = new JLabel("Your email: ", JLabel.CENTER);

        firstNameTxt = new JTextField();
        lastNameTxt = new JTextField();
        emailTxt = new JTextField();
        
        generate = new JButton("Generate");
        generateHandler = new generateBtnHandler();
        generate.addActionListener(generateHandler);
        exit = new JButton("Exit");
        exitHandler = new exitBtnHandler();
        exit.addActionListener(exitHandler);

        Container pane = getContentPane();
        pane.setLayout(new GridLayout(4, 2));
        pane.add(firstNameLbl);
        pane.add(firstNameTxt);
        pane.add(lastNameLbl);
        pane.add(lastNameTxt);
        pane.add(emailLbl);
        pane.add(emailTxt);
        pane.add(generate);
        pane.add(exit);
        setSize(400, 400);
        setVisible(true);
    }
    public class generateBtnHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String Fname, Lname;
            Fname = firstNameTxt.getText();
            Lname = lastNameTxt.getText();
            emailTxt.setText(Fname +"_"+ Lname + "@gmail.com");
        }
    }  
    public class exitBtnHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }
    public static void main (String[] args){
        GUI gui = new GUI();
    }
}
