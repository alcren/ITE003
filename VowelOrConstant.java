import java.util.Scanner;
/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 9
 * PROJECT NAME/CLASS NAME: PRELIM/VOWELORCONSTANT
 * DATE: 4/20/16
 */
public class VowelOrConstant
{
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("***Vowel or constant?***");
        System.out.print("Enter a letter: ");
        char ch = sc.next().charAt(0);
        switch (ch){
            case 'A' :
            case 'E' :
            case 'I' :
            case 'O' :
            case 'U' :
            case 'a' :
            case 'e' :
            case 'i' :
            case 'o' :
            case 'u' :
            System.out.println("The letter " + ch + " is a vowel.");
            break;
            case '0' :
            case '1' :
            case '2' :
            case '3' :
            case '4' :
            case '5' :
            case '6' :
            case '7' :
            case '8' :
            case '9' :
            System.out.println("Invalid input. Enter letters only.");
            break;
            default :
            System.out.println("The letter " + ch + " is a consonant.");
        }
        
    }
}