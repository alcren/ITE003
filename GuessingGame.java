/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 11
 * PROJECT NAME/CLASS NAME: PRELIM/NUMBERGUESSINGGAME
 * DATE: 4/26/16
 */
import java.util.Scanner;
public class GuessingGame {
    public static void main (String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        int guess = 0;
        System.out.println("\t<<NUMBER GUESSING GAME>>");
        do{
            System.out.print("Guess the number: ");
            guess = sc.nextInt();
            if (guess > 397){
                System.out.print("Too high!");
                Thread.sleep(1000);
                System.out.print('\u000c');
            }
            else if (guess < 397){
                System.out.print("Too low!");
                Thread.sleep(1000);
                System.out.print('\u000c');
            }
        } while (guess != 397);
        System.out.println("Got it!");
    }
}
