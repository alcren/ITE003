
/*
 * Title: ASCII_Art
 * Author: Efren Mercado Jr
 * Program: BS CS
 * Year Level: First Year
 * Date: 4/14/16
 */
public class ASCII_Art
{
    public static void main (String[] args){
       System.out.println("********************");
       System.out.println("*******      *******");
       System.out.println("****   ******   ****");
       System.out.println("****   ******   ****");
       System.out.println("****   ******   ****");
       System.out.println("*******      *******");
       System.out.println("********    ********");
       System.out.println("**                **");
       System.out.println("********    ********");
       System.out.println("********    ********");
       System.out.println("********    ********");
       System.out.println("*******  **  *******");
       System.out.println("*****  ******  *****");
       System.out.println("***  *********  ****");
       
    }
}
