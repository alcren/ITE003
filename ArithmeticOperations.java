
/**
 * Write a description of class ArithmeticOperations here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ArithmeticOperations
{
    public static void main(String[] args){
        //Integer variable
        int x=3, y=1;
        //double
        double a=5;
        double b=10.25;
        //character
        char z = '#';
        //boolean
        boolean c=false;
        
        
        int sum = x+y;
        int diff = x-y;
        int prod = x*y;
        int quo = x/y;
        int rem = x%y;
        System.out.println("x:"+x+"\ny:"+y+"\nz:"+z+"\na:"+a+"\nb:"+b+"\nc:"+c+"\nz:"+z);
        System.out.println("Sum: " + sum + "\nDifference: " + diff + "\nProduct: " + prod + "\nQuotient: " + quo + "\nRemainder: " + rem);
        
        double sumD = a+b;
        double diffD = a-b;
        double prodD = a*b;
        double quoD = a/b;
        double remD = a%b;
        System.out.println("Sum: " + sumD + "\nDifference: " + diffD + "\nProduct: " + prodD + "\nQuotient: " + quoD + "\nRemainder: " + remD);
    }
}
