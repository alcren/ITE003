/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 10
 * PROJECT NAME/CLASS NAME: PRELIM/LOOPEXER1
 * DATE: 4/26/16
 */
public class LoopExer1
{
    public static void main (String[] args){
        int i = 0;
        int sum = 0;
        for(i = 1; i<=10; i++){
            System.out.print(i + " ");
            sum = sum + i;
        }
        System.out.println("Sum = " + sum);
        System.out.println("\n======================================");
        i = 2;
        while (i <= 20){
            System.out.print(i + " ");
            i+=2;
        }
        System.out.println("\n======================================");
        i = 5;
        do{
        System.out.println(i + " Efren");
        i--;
        }
        while(i >= 1); 
    }
}