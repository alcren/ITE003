/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 14
 * PROJECT NAME/CLASS NAME: PRELIM/GRADINGSYSTEMARRAY
 * DATE: 05/03/16
 */
import java.util.Scanner;
public class GradingSystemArray
{
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("\t<<Grading System>>\n  ");
        //quizzes
        System.out.print("Enter number of quizzes: ");
        int nq = sc.nextInt();
        double[] quiz = new double[nq];
        for(int i = 0; i < nq; i++){
            System.out.print("Enter quiz grade " + (i+1) + ": ");
            quiz[i] = sc.nextInt();
        }
        double total_q = 0;
        for(int i = 0; i < nq; i++){
            total_q += quiz[i];
        }
        double cs_quiz = total_q / nq;
        System.out.println("Average: " + cs_quiz);
        //assignments
        System.out.print("\nEnter number of Assignments: ");
        int na = sc.nextInt();
        double[] assign = new double[na];
        for(int i = 0; i < na; i++){
            System.out.print("Enter Assignment Grade " + (i+1) + ": ");
            assign[i] = sc.nextInt();
        }
        double total_a = 0;
        for(int i = 0; i < na; i++){
            total_a += assign[i];
        }
        double cs_assign = total_a / na;
        System.out.println("Average: " + cs_assign);
        //lab activities
        System.out.print("\nEnter number of Laboratory Activities: ");
        int nla = sc.nextInt();
        double[] lab = new double[nla];
        for(int i = 0; i < nla; i++){
            System.out.print("Enter Lab grade " + (i+1) + ": ");
            lab[i] = sc.nextInt();
        }
        double total_l = 0;
        for(int i = 0; i < nla; i++){
            total_l += lab[i];
        }
        double cs_lab = total_l / nla;
        System.out.println("Average: " + cs_lab);
        double cs = cs_quiz * .6 + cs_assign * .2 + cs_lab * .2;
        //exams
        int ne = 0;
        double exam = 0;
        do{
            System.out.print("\nEnter number of Exam Grades: ");
            ne = sc.nextInt();
            if (ne == 1){
                System.out.print("Enter exam grade: ");
                exam = sc.nextDouble();
            } else if (ne == 2){
                System.out.print("Enter exam grade (lecture): ");
                double exam_lec = sc.nextDouble();
                System.out.print("Enter exam grade (laboratory): ");
                double exam_lab = sc.nextDouble();
                exam = exam_lec * .4 + exam_lab * .6;
            } else{
                System.out.println("Maximum of 2 exams and minimum of 1 is allowed.");
                Thread.sleep(1000);
            }
        }while (!(ne >= 1 && ne <= 2));
        double grade = cs * .5 + exam * .5;
        System.out.println("\nYour MIDTERM GRADE is " + grade);
        double point_grade = 0;
        if (grade > 94.00 && grade <= 100){
            point_grade = 1.00;
        }
        else if (grade > 49.99 && grade <= 55.49){
            point_grade = 3.00;
        }
        else if (grade > 55.50 && grade <= 60.99){
            point_grade = 2.75;
        }
        else if (grade > 61.00 && grade <= 65.49){
            point_grade = 2.50;
        }
        else if (grade > 65.50 && grade <= 71.99){
            point_grade = 2.25;
        }
        else if (grade > 72.00 && grade <= 77.49){
            point_grade = 2.00;
        }
        else if (grade > 77.50 && grade <= 82.99){
            point_grade = 1.75;
        }
        else if (grade > 83.00 && grade <= 88.49){
            point_grade = 1.50;
        }
        else if (grade > 88.50 && grade <= 93.99){
            point_grade = 1.25;
        }
        else 
            point_grade = 5.00;
        System.out.println("Your point grade is " + point_grade);
    }
}
