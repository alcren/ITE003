import java.util.Scanner;
public class myMethodSample
{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter first number: ");
        int num1 = sc.nextInt();
        System.out.print("Enter second number: ");
        int num2 = sc.nextInt();
        int min = minFunction(num1, num2);
        System.out.println("Minimum value is " + min);
        maxFunction(num1, num2);
    }

    /** the snippet returns the minimum between two numbers */
    public static int minFunction(int n1, int n2) {
        int min;
        if (n1 > n2)
            min = n2;
        else
            min = n1;
        return min; 
    }

    public static void maxFunction(int n1, int n2) {
        int max;
        if (n1 > n2)
            max = n1;
        else
            max = n2;
        System.out.println("Maximum value is " + max);
    }
}
