/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * ACTIVITY NUMBER: 13
 * PROJECT NAME/CLASS NAME: PRELIM/ODDEVEN
 * DATE: 05/03/16
 */
import java.util.Scanner;
public class OddEven
{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("\t<<Odd or Even Using Array>>\n");
        System.out.print("Enter number of elements: ");
        int n = sc.nextInt();
        System.out.println(" ");
        int[] myList = new int[n];
        int[] oddList = new int[n];
        int[] evenList = new int[n];
        for(int i = 0; i < n; i++){
            System.out.print("Enter element " + (i+1) + ": ");
            myList[i] = sc.nextInt();
        }
        int j = 0, k = 0;
        for(int i = 0; i < n; i++){
            if( (myList[i]%2) == 0 ) {
                evenList[j] = myList[i];
                j++;
            }
            else {
                oddList[k] = myList[i];
                k++;
            }
        }
        bubbleSort(evenList, j);
        bubbleSort(oddList, k);
        System.out.print("\nThe even numbers are: ");
        for(int i = 0; i < j; i++){
            System.out.print(evenList[i] + " ");
        }
        System.out.print("\nThe odd numbers are: ");
        for(int i = 0; i < k; i++){
            System.out.print(oddList[i] + " ");
        }
    }

    public static void bubbleSort(int[] myList, int length) {
        int n = length;
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {

                if (myList[j - 1] > myList[j]) {
                    temp = myList[j - 1];
                    myList[j - 1] = myList[j];
                    myList[j] = temp;
                }

            }
        }
    }
}
